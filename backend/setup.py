import io
from setuptools import setup, find_packages

print(find_packages(exclude=["tests*"]))

setup(
    name="textquest",
    version="0.0.1",
    author="Constantin Tsaregorodtsev",
    author_email="a@mail.ma",
    packages=find_packages(exclude=["tests*"]),
    zip_safe=False,
    include_package_data=True,
    license="AGPL3",
    description=(
        "Backend for testquest. Testquest is editor and player for a text quest. "
    ),
    long_description=io.open("README.md", encoding="utf-8").read(),  # pylint: disable=consider-using-with
    long_description_content_type="text/markdown",
    install_requires=["flask>=2.1.0", "flask_sqlalchemy>=2.5.1"],
    python_requires=">=3.6",
    url="https://gitlab.com/ktsaregorodtsev/textquest",
    project_urls={
        "Documentation": "https://gitlab.com/ktsaregorodtsev/textquest/-/wikis/home",
        "Source": "https://gitlab.com/ktsaregorodtsev/textquest/-/tree/main",
        "Issue Tracker": "https://gitlab.com/ktsaregorodtsev/textquest/-/issues?sort=created_date&state=opened",
    },
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Web Environment",
        "Framework :: Flask",
        "Programming Language :: Python",
    ],
)